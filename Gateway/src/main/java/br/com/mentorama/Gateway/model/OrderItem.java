package br.com.mentorama.Gateway.model;

public class OrderItem {

    private Product product;
    private Integer quantidade_pedido;
    private Double desconto;

    public OrderItem(Product product, Integer quantidade_pedido, Double desconto) {
        this.product = product;
        this.quantidade_pedido = quantidade_pedido;
        this.desconto = desconto;
    }

    public Double precoTotal(){
        if(product.getQuantidade_dis() > quantidade_pedido){
            return product.getPrecoComDesconto(desconto) * quantidade_pedido;
        }else{
            return product.getPrecoComDesconto(desconto) * product.getQuantidade_dis();
        }
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getQuantidade_pedido() {
        return quantidade_pedido;
    }

    public void setQuantidade_pedido(Integer quantidade_pedido) {
        this.quantidade_pedido = quantidade_pedido;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }
}
