package br.com.mentorama.Gateway.model;

public class Product {

    private Integer id = 0;
    private String name;
    private Double preco;
    private Double maxDisc;
    private Integer quantidade_dis;

    public Product(Integer id, String name, Double preco, Double maxDisc, Integer quantidade_dis) {
        this.id += 1;
        this.name = name;
        this.preco = preco;
        this.maxDisc = maxDisc;
        this.quantidade_dis = quantidade_dis;
    }

    public Double getPrecoComDesconto(final Double disconto){
        if (disconto > maxDisc){
            return preco * (1 - maxDisc);
        }else{
            return preco * (1-disconto);
        }
    }

    public Integer getQuantidade_dis(Integer quantidadeSolicitada){
        if (quantidade_dis >= quantidadeSolicitada){
            return this.quantidade_dis - quantidadeSolicitada;
        }else{
            return quantidadeSolicitada = this.quantidade_dis;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }

    public Double getMaxDisc() {
        return maxDisc;
    }

    public void setMaxDisc(Double maxDisc) {
        this.maxDisc = maxDisc;
    }

    public Integer getQuantidade_dis() {
        return quantidade_dis;
    }

    public void setQuantidade_dis(Integer quantidade_dis) {
        this.quantidade_dis = quantidade_dis;
    }
}
