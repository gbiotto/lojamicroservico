package br.com.mentorama.Gateway.controllers;

import br.com.mentorama.Gateway.model.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

public class MessagemController {
    @RestController
    @RequestMapping("/message")
    public class MessageController {

        @Autowired
        private JmsTemplate jmsTemplate;

        @PostMapping
        public void sendMessage(@RequestBody final Email email) {
            System.out.println("Enviando pedido");
            jmsTemplate.convertAndSend("pedido", email);
        }

        @PostMapping("/produto")
        public void sendMessage(@RequestBody final Email email) {
            System.out.println("Atualizando produto");
            jmsTemplate.convertAndSend("produto", email);
        }

    }
}
