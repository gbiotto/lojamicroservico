package br.com.mentorama.Gateway.configurations;

import org.springframework.stereotype.Component;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

@Component
public class JMSErrorhandler implements ErrorHandler, org.springframework.util.ErrorHandler {

    @Override
    public void handleError(Throwable throwable){
        System.out.println("Error processing message " + throwable.getMessage());
    }

    @Override
    public void warning(SAXParseException exception) throws SAXException {
        System.out.println("Error processing message " + exception.getMessage());
    }

    @Override
    public void error(SAXParseException exception) throws SAXException {
        System.out.println("Error processing message " + exception.getMessage());
    }

    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        System.out.println("Error processing message " + exception.getMessage());
    }
}
