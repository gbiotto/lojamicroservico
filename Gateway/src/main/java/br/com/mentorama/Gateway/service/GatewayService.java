package br.com.mentorama.Gateway.service;

import br.com.mentorama.Gateway.model.Email;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class GatewayService {

    @JmsListener(destination = "pedido", containerFactory = "myFactory")
    public void receiveMessage(Email email){
        if(email.getTo() == null || email.getBody() == null || email.getOrderItems() == null){
            throw new RuntimeException("Invalid email");
        }
        System.out.println("Requerimento de novo pedido<" + email + ">");
    }

    @JmsListener(destination = "produto", containerFactory = "myFactory")
    public void receiveMessage(Email email){
        if(email.getTo() == null || email.getBody() == null){
            throw new RuntimeException("Invalid email");
        }
        System.out.println("Requerimento de produto<" + email + ">");
    }

}
