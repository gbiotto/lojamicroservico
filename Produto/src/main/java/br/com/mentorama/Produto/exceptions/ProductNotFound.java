package br.com.mentorama.LojaMentorama.exceptions;

public class ProductNotFound extends  InterruptedException{

    public ProductNotFound(){
        System.out.println("Falha ao encontrar produto");
    }

}
