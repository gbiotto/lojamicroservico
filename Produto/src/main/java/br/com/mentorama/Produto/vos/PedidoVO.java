package br.com.mentorama.Produto.vos;

public class PedidoVO {

    private String id_pedido;
    private Double desconto_aplicado;
    private Double valor;
    private Integer id_cliente;

    public String getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(String id_pedido) {
        this.id_pedido = id_pedido;
    }

    public Double getDesconto_aplicado() {
        return desconto_aplicado;
    }

    public void setDesconto_aplicado(Double desconto_aplicado) {
        this.desconto_aplicado = desconto_aplicado;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Integer getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Integer id_cliente) {
        this.id_cliente = id_cliente;
    }

    @Override
    public String toString() {
        return "PedidoVO{" +
                "id_pedido='" + id_pedido + '\'' +
                ", desconto_aplicado=" + desconto_aplicado +
                ", valor=" + valor +
                ", id_cliente=" + id_cliente +
                '}';
    }
}
