package br.com.mentorama.Produto.service;

import br.com.mentorama.Produto.ProductClienteAPI;
import br.com.mentorama.Produto.entities.ProdutoEntity;
import br.com.mentorama.Produto.model.Product;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductClienteAPI cliente;
    private List<ProdutoEntity> produtoEntities = new ArrayList<>();
    private ProdutoRepository produtoRepository;

    public List<Product> findAll(){
        return cliente.findAll();
    }

    public List<Product> findPrice(){
        return cliente.findPrice();
    }

    public List<Product> findById(){ return cliente.findById();}

    public List<ProdutoEntity> verificarId(ProdutoEntity produto){
        if (produto.getId_produto() != null && produtoEntities.contains(produto.getId_produto())){
                System.out.println("Produto ja cadastrado");
            }
        else{
            System.out.println("Erro ao localizar id do produto");
        }
        return this.produtoEntities;
    }

    public CompletableFuture<List<Products>> findAll(){
        System.out.println("Thread: " + Thread.currentThread().getName());
        return  productRepository.findAllBy();
    }

    public CompletableFuture<Optional<Products>> findById(Integer uuid){
        System.out.println("Thread: " + Thread.currentThread().getName());
        return productRepository.findOneById(uuid);
    }

    public CompletableFuture<Products> save(final Products products){
        System.out.println("Thread: " + Thread.currentThread().getName());
        return CompletableFuture.completedFuture(productRepository.save(products));
    }

    public void delete(Integer id){
        productRepository.deleteById(id);
    }
}
