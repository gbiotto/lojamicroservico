package br.com.mentorama.Produto;

import br.com.mentorama.Produto.model.Product;
import br.com.mentorama.Produto.model.ProductDTO;
import javax.annotation.security.RolesAllowed;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import java.util.List;

@Component
public class ProductClienteAPI {

    @Value("${vendaProduto.url}")
    private String url;
    private String id;

    @RolesAllowed("user")
    public List<Product> findAll(){
        ResponseEntity<ProductDTO> responseEntity = new RestTemplate().getForEntity(url, ProductDTO.class);

        return responseEntity.getBody().getProducts();
    }

    @RolesAllowed("user")
    public List<Product> findPrice(){
        ResponseEntity<ProductDTO> responseEntity =
                new RestTemplate().getForEntity(url, ProductDTO.class);
        return responseEntity.getBody().getProducts();
    }

    public List<Product> findById() {
        ResponseEntity<ProductDTO> responseEntity = new RestTemplate().getForEntity(id, ProductDTO.class);
        return responseEntity.getBody().getProducts();
    }
}
