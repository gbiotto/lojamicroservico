package br.com.mentorama.Produto.repository;

import br.com.mentorama.Produto.entities.ProdutoEntity;
import br.com.mentorama.Produto.vos.ProdutoVO;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface ProdutoRepository extends JpaRepository<ProdutoEntity, Long> {
    List<ProdutoEntity> findAllProduto(String produto);

    List<ProdutoVO> findById();

    void deleteById(final Integer id);
}
