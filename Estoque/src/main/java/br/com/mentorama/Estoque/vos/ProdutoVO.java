package br.com.mentorama.Estoque.vos;

public class ProdutoVO {
    private Integer id_produto;
    private String nome_produto;
    private String descricao;
    private Double valor;
    private Double desc_max;
    private Integer qtd_disponivel;

    public Integer getId_produto() {
        return id_produto;
    }

    public void setId_produto(Integer id_produto) {
        this.id_produto = id_produto;
    }

    public String getNome_produto() {
        return nome_produto;
    }

    public void setNome_produto(String nome_produto) {
        this.nome_produto = nome_produto;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Double getDesc_max() {
        return desc_max;
    }

    public void setDesc_max(Double desc_max) {
        this.desc_max = desc_max;
    }

    public Integer getQtd_disponivel() {
        return qtd_disponivel;
    }

    public void setQtd_disponivel(Integer qtd_disponivel) {
        this.qtd_disponivel = qtd_disponivel;
    }

    @Override
    public String toString() {
        return "ProdutoVO{" +
                "id_produto=" + id_produto +
                ", nome_produto='" + nome_produto + '\'' +
                ", descricao='" + descricao + '\'' +
                ", valor=" + valor +
                ", desc_max=" + desc_max +
                ", qtd_disponivel=" + qtd_disponivel +
                '}';
    }
}
