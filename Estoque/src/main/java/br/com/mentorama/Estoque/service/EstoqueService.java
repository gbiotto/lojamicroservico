package br.com.mentorama.Estoque.service;

import br.com.mentorama.Estoque.entities.ProdutoEntity;
import br.com.mentorama.Estoque.model.OrderItem;
import br.com.mentorama.Estoque.model.Product;
import org.springframework.stereotype.Service;

@Service
public class EstoqueService {

    private ProdutoEntity produtoEntity;
    private OrderItem orderItem;

    public void quantidadeDisponivel(final Product product){
        if (product.getQuantidade_dis() >= orderItem.getQuantidade_pedido()){
            System.out.println("Quantidade disponivel para venda");
        }
        else{
            System.out.println("Quantidade indisponivel para venda");
        }
    }

}
