package br.com.mentorama.Estoque.entities;

import javax.persistence.*;

@Entity
@Table(name = "produto")
public class ProdutoEntity {

    @Id
    @GeneratedValue
    private Integer id_produto;

    @Column(name = "nome_produto", nullable = false)
    private String nome_produto;

    @Column(name = "descricao", nullable = false)
    private String descricao;

    @Column(name = "valor", nullable = false)
    private Double valor;

    @Column(name = "desc_max", nullable = false)
    private Double desc_max;

    @Column(name = "qtd_disponivel", nullable = false)
    private Integer qtd_disponivel;

    public ProdutoEntity(Integer id_produto, String nome_produto, String descricao, Double valor, Double desc_max, Integer qtd_disponivel) {
        this.id_produto = id_produto;
        this.nome_produto = nome_produto;
        this.descricao = descricao;
        this.valor = valor;
        this.desc_max = desc_max;
        this.qtd_disponivel = qtd_disponivel;
    }

    public Integer getId_produto() {
        return id_produto;
    }

    public void setId_produto(Integer id_produto) {
        this.id_produto = id_produto;
    }

    public String getNome_produto() {
        return nome_produto;
    }

    public void setNome_produto(String nome_produto) {
        this.nome_produto = nome_produto;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Double getDesc_max() {
        return desc_max;
    }

    public void setDesc_max(Double desc_max) {
        this.desc_max = desc_max;
    }

    public Integer getQtd_disponivel() {
        return qtd_disponivel;
    }

    public void setQtd_disponivel(Integer qtd_disponivel) {
        this.qtd_disponivel = qtd_disponivel;
    }
}
