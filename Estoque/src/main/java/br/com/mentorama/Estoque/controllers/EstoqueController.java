package br.com.mentorama.Estoque.controllers;

import br.com.mentorama.Estoque.entities.ProdutoEntity;
import br.com.mentorama.Estoque.repository.ProdutoRepository;
import br.com.mentorama.Estoque.vos.ProdutoVO;
import java.util.stream.Collectors;
import javax.annotation.security.RolesAllowed;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/estoque")
public class EstoqueController {

    private final ProdutoRepository produtoRepository;

    public EstoqueController(ProdutoRepository produtoRepository) {
        this.produtoRepository = produtoRepository;
    }

    @RolesAllowed("admin")
    @PostMapping
    private ProdutoVO toProdutoVO(ProdutoEntity produto){
        ProdutoVO produtoVO = new ProdutoVO();

        produtoVO.setId_produto(produto.getId_produto());
        produtoVO.setNome_produto(produtoVO.getNome_produto());
        produtoVO.setDescricao(produtoVO.getDescricao());
        produtoVO.setValor(produtoVO.getValor());
        produtoVO.setDesc_max(produtoVO.getDesc_max());
        produtoVO.setQtd_disponivel(produtoVO.getQtd_disponivel());

        return produtoVO;
    }

    @GetMapping
    private List<ProdutoVO> findAll(@RequestParam("page") Integer page,
                                    @RequestParam("pageSize") Integer pageSize){
        return this.produtoRepository.findAll(PageRequest.of(page, pageSize,
                Sort.by("nome_pruduto")))
                .stream()
                .map(this::toProdutoVO)
                .collect(Collectors.toList());

    }

    @GetMapping
    private List<ProdutoVO> findById(){
        return this.produtoRepository.findById().stream().collect(Collectors.toList());
    }

    @RolesAllowed("admin")
    @PutMapping
    public void update(@RequestBody ProdutoEntity produtoEntity){
        this.produtoRepository.save(produtoEntity);
    }

}
