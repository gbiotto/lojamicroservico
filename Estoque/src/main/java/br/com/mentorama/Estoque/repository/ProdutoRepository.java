package br.com.mentorama.Estoque.repository;

import br.com.mentorama.Estoque.entities.ProdutoEntity;
import br.com.mentorama.Estoque.vos.ProdutoVO;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface ProdutoRepository extends JpaRepository<ProdutoEntity, Long> {
    List<ProdutoEntity> findAllProduto(String produto);

    List<ProdutoVO> findById();
}
