package br.com.mentorama.Pedido.service;

import br.com.mentorama.Pedido.model.Order;
import br.com.mentorama.Pedido.model.OrderItem;
import java.util.List;

public class OrderCalculator {

    public Double calculateOrder(final Order order){
        return order.getItems().stream().mapToDouble(OrderItem::precoTotal).sum();
    }

    public Double calculateMultipleOrders(final List<Order> orders){
        return orders.stream().mapToDouble(order -> calculateOrder(order)).sum();
    }
}
