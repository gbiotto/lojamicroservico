package br.com.mentorama.Pedido.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "itens_pedido")
public class ItensPedidoEntity {

    @OneToOne(mappedBy = "id_pedido")
    private PedidoEntity id_pedido;

    @OneToOne(mappedBy = "id_produto")
    private ProdutoEntity id_produto;

    @Column(name = "quantidade", nullable = false)
    private Integer quantidade;

    public ItensPedidoEntity(PedidoEntity id_pedido, ProdutoEntity id_produto, Integer quantidade) {
        this.id_pedido = id_pedido;
        this.id_produto = id_produto;
        this.quantidade = quantidade;
    }

    public PedidoEntity getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(PedidoEntity id_pedido) {
        this.id_pedido = id_pedido;
    }

    public ProdutoEntity getId_produto() {
        return id_produto;
    }

    public void setId_produto(ProdutoEntity id_produto) {
        this.id_produto = id_produto;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
