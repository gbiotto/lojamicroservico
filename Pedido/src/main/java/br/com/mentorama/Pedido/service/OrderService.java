package br.com.mentorama.Pedido.service;

import br.com.mentorama.Pedido.entities.PedidoEntity;
import br.com.mentorama.Pedido.repository.PedidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    private PedidoRepository pedidoRepository;

    public OrderService(PedidoRepository pedidoRepository) {
        this.pedidoRepository = pedidoRepository;
    }

    public List<PedidoEntity>findAll(){
        return this.pedidoRepository.findAll();
    }

    public void add(final PedidoEntity pedido){
        if(pedido.getId_pedido() != null){
            pedido.setId_pedido(pedido.getId_pedido());
        }
    }
}
