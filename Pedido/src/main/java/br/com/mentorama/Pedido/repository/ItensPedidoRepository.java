package br.com.mentorama.Pedido.repository;

import br.com.mentorama.Pedido.entities.ItensPedidoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface ItensPedidoRepository extends JpaRepository<ItensPedidoEntity, Long> {

    @Query(value = "select * from ItensPedidoEntity ip left join p.produto on ip.id_produto = p.id_produto")
    List<ItensPedidoEntity> findItensPedido();


}
