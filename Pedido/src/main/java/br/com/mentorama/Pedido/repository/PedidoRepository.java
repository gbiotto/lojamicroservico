package br.com.mentorama.Pedido.repository;

import br.com.mentorama.Pedido.entities.PedidoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface PedidoRepository extends JpaRepository<PedidoEntity, Long> {

    @Query(value = "select * from PedidoEntity p left jon c.cliente on p.id_cliente = ?1", nativeQuery = true)
    List<PedidoEntity> findPedidoByIdCliente(Integer id_cliente);

}
