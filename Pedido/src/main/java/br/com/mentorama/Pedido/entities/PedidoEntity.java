package br.com.mentorama.Pedido.entities;

import javax.persistence.*;

@Entity
@Table(name = "pedido")
public class PedidoEntity {

    @Id
    @GeneratedValue
    private Integer id_pedido;

    @Column(name = "desconto_aplicado")
    private double desconto_aplicado;

    @Column(name = "valor", nullable = false)
    private double valor;

    @OneToMany(mappedBy = "id_cliente")
    private ClienteEntity id_cliente;

    public PedidoEntity(Integer id_pedido, double desconto_aplicado, double valor, ClienteEntity id_cliente) {
        this.id_pedido = id_pedido;
        this.desconto_aplicado = desconto_aplicado;
        this.valor = valor;
        this.id_cliente = id_cliente;
    }

    public Integer getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(Integer id_pedido) {
        this.id_pedido = id_pedido;
    }

    public double getDesconto_aplicado() {
        return desconto_aplicado;
    }

    public void setDesconto_aplicado(double desconto_aplicado) {
        this.desconto_aplicado = desconto_aplicado;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

}
