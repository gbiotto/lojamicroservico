package br.com.mentorama.Pedido.entities;

import javax.persistence.*;

@Entity
@Table(name = "cliente")
public class ClienteEntity {

    @Id
    @GeneratedValue
    private Integer id_cliente;

    @Column(name = "nome_cliente", nullable = false)
    private String nome_cliente;

    @Column(name = "telefone", nullable = false)
    private Integer telefone;


    public ClienteEntity(Integer id_cliente, String nome_cliente, Integer telefone) {
        this.id_cliente += 1;
        this.nome_cliente = nome_cliente;
        this.telefone = telefone;
    }

    public Integer getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Integer id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getNome_cliente() {
        return nome_cliente;
    }

    public void setNome_cliente(String nome_cliente) {
        this.nome_cliente = nome_cliente;
    }

    public Integer getTelefone() {
        return telefone;
    }

    public void setTelefone(Integer telefone) {
        this.telefone = telefone;
    }
}
