package br.com.mentorama.Pedido.controller;


import br.com.mentorama.Pedido.entities.ItensPedidoEntity;
import br.com.mentorama.Pedido.entities.PedidoEntity;
import br.com.mentorama.Pedido.repository.ItensPedidoRepository;
import br.com.mentorama.Pedido.repository.PedidoRepository;
import br.com.mentorama.Pedido.repository.ProdutoRepository;
import javax.annotation.security.RolesAllowed;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;

public class PedidoController {

    private final ProdutoRepository produtoRepository;
    private final PedidoRepository pedidoRepository;
    private final ItensPedidoRepository itensPedidoRepository;

    public PedidoController(ProdutoRepository produtoRepository, PedidoRepository pedidoRepository, ItensPedidoRepository itensPedidoRepository) {
        this.produtoRepository = produtoRepository;
        this.pedidoRepository = pedidoRepository;
        this.itensPedidoRepository = itensPedidoRepository;
    }

    private List<ItensPedidoEntity> itensPedidoEntities;

    @RolesAllowed("user")
    @GetMapping("/{id_pedido}")
    public PedidoEntity findByIdPedido(@PathVariable("id_pedido") final Long id_pedido){
        return this.pedidoRepository.getById(id_pedido);
    }
}
