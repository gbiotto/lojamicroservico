package br.com.mentorama.Pedido.repository;

import br.com.mentorama.Pedido.entities.ProdutoEntity;
import br.com.mentorama.Pedido.vos.ProdutoVO;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface ProdutoRepository extends JpaRepository<ProdutoEntity, Long> {
    List<ProdutoEntity> findAllProduto(String produto);

    List<ProdutoVO> findById();
}
